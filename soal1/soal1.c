#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#define maxSize 1024
char dirpath[maxSize];

//melakukan encode/decode
char * Clem_code(char src[]) {
  char str[maxSize];
  strcpy(str, src);
  int i;
  for (i = 0; i < strlen(src); i++) {
	str[i] = src[strlen(src) - i - 1];
  }
  char * res = str;
  return res;
}

char * find_path(const char * path) {
  char fpath[2 * maxSize];
  bool flag, toMirror = 0;

  char * strMir;
  if (strcmp(path, "/"))
  {
	strMir = strstr(path, "/Clem_");
	if (strMir) {
  	toMirror = 1;
  	strMir++;
	}
  }

  if (strcmp(path, "/") == 0) {
	sprintf(fpath, "%s", dirpath);
  } else if (toMirror) {
	char pathOrigin[maxSize] = "";
	char t[maxSize];

	strncpy(pathOrigin, path, strlen(path) - strlen(strMir));
	strcpy(t, strMir);

	char * selectedFile;
	char * rest = t;

	flag = 0;

	while ((selectedFile = strtok_r(rest, "/", & rest))) {
  	if (!flag) {
    	strcat(pathOrigin, selectedFile);
    	flag = 1;
    	continue;
  	}

  	char checkType[2 * maxSize];
  	sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
  	strcat(pathOrigin, "/");

  	if (strlen(checkType) == strlen(path)) {
    	char pathFolder[2 * maxSize];
    	sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

    	DIR * dp = opendir(pathFolder);
    	if (!dp) {
      	char * ext;
      	ext = strrchr(selectedFile, '.');

      	char fileName[maxSize] = "";
      	if (ext)
      	{
        	strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
        	sprintf(fileName, "%s%s", Clem_code(fileName), ext);
      	} else if (toMirror)
        	strcpy(fileName, Clem_code(selectedFile));

      	printf("%s\n", fileName);
      	strcat(pathOrigin, fileName);
    	} else
      	strcat(pathOrigin, Clem_code(selectedFile));

  	} else {
    	strcat(pathOrigin, Clem_code(selectedFile));
  	}
	}
	sprintf(fpath, "%s%s", dirpath, pathOrigin);
  } else
	sprintf(fpath, "%s%s", dirpath, path);

  char * fpath_to_return = fpath;
  return fpath_to_return;
}

static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {
  char fpath[maxSize];
  bool toMirror = strstr(path, "/Clem_");
  strcpy(fpath, find_path(path));

  int res = 0;
  DIR * dp;
  struct dirent * de;

  (void) offset;
  (void) fi;

  dp = opendir(fpath);
  if (dp == NULL)
	return -errno;

  while ((de = readdir(dp)) != NULL) {
	struct stat st;
	memset( & st, 0, sizeof(st));
	st.st_ino = de -> d_ino;
	st.st_mode = de -> d_type << 12;

	if (strcmp(de -> d_name, ".") == 0 || strcmp(de -> d_name, "..") == 0) {
  	res = (filler(buf, de -> d_name, & st, 0));
	} else if (toMirror) {
  	if (de -> d_type & DT_DIR) {
    	char temp[maxSize];
    	strcpy(temp, de -> d_name);

    	res = (filler(buf, Clem_code(temp), & st, 0));
  	} else {
    	char * ext;
    	ext = strrchr(de -> d_name, '.'); //abc.112.1

    	char fileName[maxSize] = "";
    	if (ext) {
      	strncpy(fileName, de -> d_name, strlen(de -> d_name) - strlen(ext));
      	strcpy(fileName, Clem_code(fileName)); //encode --> edocne.log
      	strcat(fileName, ext);
    	} else {
      	strcpy(fileName, Clem_code(de -> d_name));
    	}
    	res = (filler(buf, fileName, & st, 0));
  	}
	} else
  	res = (filler(buf, de -> d_name, & st, 0));

	if (res != 0)
  	break;
  }
  closedir(dp);
  return 0;
}

static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
  char fpath[maxSize];
  strcpy(fpath, find_path(path));

  int fd;
  int res;

  (void) fi;
  fd = open(fpath, O_RDONLY);
  if (fd == -1)
	return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1)
	res = -errno;

  close(fd);
  return res;
}

static int xmp_getattr(const char * path, struct stat * stbuf) {
  char fpath[maxSize];
  strcpy(fpath, find_path(path));

  int res;
  res = lstat(fpath, stbuf);
  if (res == -1)
	return -errno;

  return 0;
}

static struct fuse_operations xmp_oper = {
  .getattr = xmp_getattr,
  .readdir = xmp_readdir,
  .read = xmp_read,
};

int main(int argc, char * argv[]) {
  char * username = getenv("USER");

  sprintf(dirpath, "/home/%s/Desktop", username);

  umask(0);

  return fuse_main(argc, argv, & xmp_oper, NULL);
}