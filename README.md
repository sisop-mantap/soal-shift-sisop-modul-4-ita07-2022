# soal-shift-sisop-modul-4-ita07-2022 <br>

![Stay Hallal](img/memehallal.jpg)

# Kelompok ITA07
1. Maulanal Fatihil A. M.   5027201031
2. Cherylene Trevina        5027201033
3. M. Hilmi Azis            5027201049

# Daftar Isi
* [Daftar isi](#daftar-isi)
* [Nomor 1](#nomor-1)
* [Penyelesaian Nomor 1](#penyelesaian-nomor-1)
    * [Nomor 1a](#1a)
    * [Nomor 1b](#1b)
    * [Nomor 1c](#1c)
    * [Nomor 1d](#1d)
    * [Nomor 1e](#1e)
* [Nomor 2](#nomor-2)
* [Penyelesaian Nomor 2](#penyelesaian-nomor-2)
    * [Nomor 2a](#2a)
    * [Nomor 2b](#2b)
    * [Nomor 2c](#2c)
    * [Nomor 2d](#2d)
    * [Nomor 2e](#2e)
    * [Nomor 2f](#2f)
    * [Nomor 2g](#2g)
    * [Screenshot output 2](#output-soal-nomor-2)
* [Nomor 3](#nomor-3)
* [Penyelesaian Nomor 3](#penyelesaian-nomor-3)

# Nomor 1
[Daftar isi](#daftar-isi)

# Penyelesaian Nomor 1
[Daftar isi](#daftar-isi)


# Nomor 2
[Daftar isi](#daftar-isi)

# Penyelesaian Nomor 2
[Daftar isi](#daftar-isi)


# Nomor 3
[Daftar isi](#daftar-isi)
3. Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya,     lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :.
a.	Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
b.	Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.
c.	Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
d.	Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).
e.	Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
	Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan 
    menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

# Penyelesaian Nomor 3
[Daftar isi](#daftar-isi)
# Soal 3

# 3A
Pertama kami membuat directory dengan awalan "nam_do-saq_"maka akan menjadi directory yang spesial
```c
static int xmp_rename(const char *from, const char *to) {
    int res; 
    char fpath[1000], tpath[1000];
    if (strcmp(from, "/") == 0) from = direktori, sprintf(fpath, "%s", from);
    else sprintf(fpath, "%s%s", direktori, from);
    if (strcmp(to, "/") == 0) to = direktori, sprintf(tpath, "%s", to);
    else sprintf(tpath, "%s%s", direktori, to); 
    struct stat path_stat;
    stat(fpath, &path_stat);
    if (!S_ISREG(path_stat.st_mode)) {
        // jika folder terencode dan mau didecode & eename Animeku ke IAN
        if (isAnimeku(fpath) && isNamdosaq(tpath)) {
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(isAnimeku(fpath) && isIAN(tpath)) {
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        // Rename IAN ke Animeku
        else if (isIAN(fpath) && isAnimeku(tpath)) {
            encodeFolderRekursif(fpath,0);
            sistemLog(fpath,tpath,1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (isNamdosaq(fpath) && isAnimeku(tpath)) {
            encodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (isAnimeku(fpath) && !isAnimeku(tpath)) {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", itung);
        }
        // Jika folder terdecode dan mau diencode
        else if (!isAnimeku(fpath) && isAnimeku(tpath)) {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", itung);
        //mulai no 2
        }
        else if(isAnimeku(fpath) && isIAN(tpath)) {
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if(isNamdosaq(fpath) && isIAN(tpath)) {
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if(isIAN(fpath) && isAnimeku(tpath)) {
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(isIAN(fpath) && isNamdosaq(tpath)) {
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(isIAN(fpath) && !isIAN(tpath)) {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", itung);
        }
        else if(!isIAN(fpath) && isIAN(tpath)) {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", itung);
        }

        //no3
        // rename Namdosaq to Animeku
        else if (isNamdosaq(fpath) && isAnimeku(tpath)) {
            encryptSpecial(fpath);
        }
        // rename Namdosaq to IAN
        else if (isNamdosaq(fpath) && isIAN(tpath)) {
            encryptSpecial(fpath);
        }
        // rename Animeku to Namdosaq
        else if (isAnimeku(fpath) && isNamdosaq(tpath)) {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }
        // rename IAN to Namdosaq
        else if (isIAN(fpath) && isNamdosaq(tpath)) {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }
    }
    res=rename(fpath, tpath);
    if(res == -1) return -errno;
    return 0;
```
# 3B 
Jika suatu directory di-rename dengan awalan "nam_do-saq_", maka akan menjadi directory spesial. Sesuai dengan jawaban pada poin a, karena hanya setiap directory dengan awalan "nam_do-saq_"

# 3C 
Jika suatu directory di-rename dengan tanpa awalan "nam_do-saq_", maka akan menjadi directory biasa. Sesuai dengan jawaban pada poin a, karena hanya setiap directory dengan awalan "nam_do-saq_"

# 3D 
Directory spesial mengembalikan enkripsi/encoding dari directory "Animeku_" dan "IAN_". 

# 3E
Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
```c
void decryptSpecial(char *filepath) {
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat stat_path;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL) {
		if (stat(dp->d_name, &stat_path) < 0);
		else if (S_ISDIR(stat_path.st_mode)) {
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0) continue;
            sprintf(dirPath,"%s/%s",filepath, dp->d_name);
            decryptSpecial(dirPath);
		}
		else {
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dp->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertDecimal(ext+1);
			for(int i=0; i<strlen(fname)-strlen(ext); i++) clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			decimalToBiner(dec, bin, strlen(clearPath)-strlen(ext2));
            getDecimal(clearPath, bin, normalcase);
            printf(newFilePath,"%s/%s",filepath,normalcase);
            rename(filePath, newFilePath);
		}
	}
    closedir(dir);
}
```