#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/xattr.h>
#include <sys/wait.h>
#include <pthread.h>
#define maxSize 1024
static const char * dirpath = "/home/kali/Desktop";

static int xmp_getattr(const char * path, struct stat * stbuf) {
  int res;
  char fpath[maxSize];
  sprintf(fpath, "%s/%s", dirpath, path);
  res = lstat(fpath, stbuf);
  if (res != 0) {
	return -ENOENT;
  }

  return 0;
}

void createLog(const char *path){
  FILE * file = fopen("encode.log", "a");
  char str[2 * maxSize];
  time_t t;
  time(&t);
  char *currentTime = ctime(&t);
  strtok(currentTime, "\n");

  sprintf(str, "%s: %s\n", currentTime, path);
  fprintf(file, "%s", str);
  fclose(file);
}

static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler,
  off_t offset, struct fuse_file_info * fi) {
  int res;
  DIR * dp;
  struct dirent * de;

  (void) offset;
  (void) fi;
char fpath[maxSize];

  if (strcmp(path, "/") == 0) {
	sprintf(fpath, "%s", dirpath);
  } else {
	sprintf(fpath, "%s%s", dirpath, path);
  }

  createLog(path);

  dp = opendir(fpath);
  if (dp == NULL)
	return -errno;

  while ((de = readdir(dp)) != NULL) {
	struct stat st;
	memset( & st, 0, sizeof(st));
	st.st_ino = de -> d_ino;
	st.st_mode = de -> d_type << 12;

	res = (filler(buf, de -> d_name, & st, 0));
	if (res != 0) break;
  }

  closedir(dp);
  return 0;
}

static int xmp_read(const char * path, char * buf, size_t size, off_t offset,
  struct fuse_file_info * fi) {
  char fpath[maxSize];

  if (strcmp(path, "/") == 0) {
	sprintf(fpath, "%s", dirpath);
  } else {
	sprintf(fpath, "%s%s", dirpath, path);
  }
  int res = 0;
  int fd = 0;

  (void) fi;
  fd = open(fpath, O_RDONLY);
  if (fd == -1)
	return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1)
	res = -errno;

  close(fd);
  return res;
}

static struct fuse_operations xmp_oper = {
  .getattr = xmp_getattr,
  .readdir = xmp_readdir,
  .read = xmp_read,
};

int main(int argc, char * argv[]) {
  // umask(0);
  return fuse_main(argc, argv, & xmp_oper, NULL);
}